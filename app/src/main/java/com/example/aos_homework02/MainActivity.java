package com.example.aos_homework02;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    User user=new User();
    boolean isAllFieldsChecked = false;
    private final static int REQUEST_CODE_1 = 1;

    String []jobs = {"គ្រូបង្រៀន", "និស្សិត"};

    EditText txtHeader, txtEmail, txtWebsite, txtSpinner;
    Button buttonSave,buttonUpload;
    RadioButton ButtonChoose;
    RadioGroup radioGroup;
    ImageView imageView;
    int SELECT_PICTURE = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        Spinner spinner = (Spinner) findViewById(R.id.mySpinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Log.d("TAG", "onItemClick: " + jobs[position]);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.d("TAG", "onItemClick: Nothing to do");
            }
        });

        buttonSave = findViewById(R.id.btnSave);
        txtHeader = findViewById(R.id.txtHeader);
        txtEmail = findViewById(R.id.txtEmail);
        txtWebsite = findViewById(R.id.txtWebsite);
        buttonUpload = findViewById(R.id.btnUpload);
        imageView = findViewById(R.id.imageView);


        buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageChooser();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isAllFieldsChecked = CheckAllFields();

                if (isAllFieldsChecked) {
                    user.setHeader(txtHeader.getText().toString());
                    user.setEmail(txtEmail.getText().toString());
                    user.setWebsite(txtWebsite.getText().toString());

                    Intent intent = new Intent(MainActivity.this, SaveActivity.class);
                    intent.putExtra("Header", user.header);
                    intent.putExtra("Email", user.email);
                    intent.putExtra("Website", user.website);
                    startActivityForResult(intent,REQUEST_CODE_1 );
                }
            }
        });
    }

    void imageChooser() {

        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_PICTURE);
    }

    public void onclickbuttonMethod(View v){
        int selectedId = radioGroup.getCheckedRadioButtonId();
        ButtonChoose = (RadioButton) findViewById(selectedId);
        if (selectedId == -1) {
            Toast.makeText(MainActivity.this, "សូមរើសប្រភព", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, ButtonChoose.getText(), Toast.LENGTH_SHORT).show();
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_1:
                if (resultCode == RESULT_OK) {
                    String messageReturn = data.getStringExtra("message_return");
                    Context context = getApplicationContext();
                    CharSequence text = messageReturn;
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                }

        }

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    imageView.setImageURI(selectedImageUri);
                }
            }

        }
    }


    private boolean CheckAllFields() {
        if (txtHeader.length() == 0) {
            txtHeader.setError("ត្រូវដាក់ប្រធានបទ");
            return false;
        }

        if (txtWebsite.length() == 0) {
            txtWebsite.setError("ត្រូវដាក់Link");
            return false;
        }

        if (txtEmail.length() == 0) {
            txtEmail.setError("ត្រូវដាក់អុីម៉ែលរ");
            return false;
        }

        if(radioGroup.getCheckedRadioButtonId()==-1)
        {
            Toast.makeText(getApplicationContext(), "សូមជ្រើសរើស", Toast.LENGTH_SHORT).show();
        }
        else
        {
            int selectedId = radioGroup.getCheckedRadioButtonId();
            ButtonChoose = (RadioButton)findViewById(selectedId);
            Toast.makeText(getApplicationContext(), ButtonChoose.getText().toString()+" is selected", Toast.LENGTH_SHORT).show();
        }

        return true;
    }
}