package com.example.aos_homework02;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SaveActivity extends AppCompatActivity {

    TextView Header, Email, Website;
    RadioGroup dpRadio;
    Spinner dpSpinner;
    ImageView dpImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);

        Header = findViewById(R.id.Header);
        Email = findViewById(R.id.Email);
        Website = findViewById(R.id.Link);

        Intent intent = getIntent();
        String header = intent.getStringExtra("Header");
        String email = intent.getStringExtra("Email");
        String website = intent.getStringExtra("Website");

        Header.setText(header);
        Email.setText(email);
        Website.setText(website);

        Button passDataTargetReturnDataButton = findViewById(R.id.btnBack);
        passDataTargetReturnDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("message_return", "Please create a new Information");
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}